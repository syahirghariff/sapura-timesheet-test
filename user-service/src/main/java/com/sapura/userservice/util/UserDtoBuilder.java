package com.sapura.userservice.util;

import com.sapura.userservice.model.User;
import org.sapura.dto.UserDto;

public class UserDtoBuilder {

    public static UserDto buildUserDto(User user){
        return UserDto.builder()
                .id(user.getId())
                .name(user.getName())
                .build();
    }
}
