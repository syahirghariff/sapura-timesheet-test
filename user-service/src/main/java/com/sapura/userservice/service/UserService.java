package com.sapura.userservice.service;

import org.sapura.dto.UserDto;

import java.util.List;

public interface UserService {

    List<UserDto> findAllUser();

    UserDto findById(Integer id);
}
