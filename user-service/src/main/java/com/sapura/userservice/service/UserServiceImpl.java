package com.sapura.userservice.service;

import com.sapura.userservice.repository.UserRepository;
import com.sapura.userservice.util.UserDtoBuilder;
import org.sapura.dto.UserDto;
import org.sapura.exception.RecordNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    public UserServiceImpl (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<UserDto> findAllUser() {
        return userRepository.findAll()
                .stream()
                .map(UserDtoBuilder::buildUserDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserDto findById(Integer id) {
        return userRepository.findById(id)
                .map(UserDtoBuilder::buildUserDto)
                .orElseThrow(()-> new RecordNotFoundException("User cannot be found"));
    }

}
