package org.sapura.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TimesheetDto {
    private int id;
    private String project;
    private String task;
    private UserDto assignedTo;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private StatusDto status;
}
