import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TimesheetDialogComponent } from '../timesheet-dialog/timesheet-dialog.component';
import { TimesheetService } from '../service/timesheet-service';

@Component({
  selector: 'app-timesheet-list',
  templateUrl: './timesheet-list.component.html',
  styleUrl: './timesheet-list.component.css'
})
export class TimesheetListComponent implements OnInit{

  timesheet_DS: any = null; 
  timesheet_columns: string[] = ['project', 'task', 'assignedTo', 'dateFrom', 'dateTo', 'status', 'action']
  
  taskInput: any = null;

  constructor(public dialog: MatDialog, private timesheetSvc: TimesheetService){}

  ngOnInit(): void {
      this.getAllTimesheet();
  }

  getAllTimesheet(task?: string){
    this.timesheetSvc.getAllTimesheet(task).subscribe({
      next: (resp:any)=> {
        this.timesheet_DS = resp; 
    }, error: ()=>{
      //do error
     }});
  }

  onSearch(){
    this.getAllTimesheet(this.taskInput)
  }




  openDialog(): void {
    const dialogRef = this.dialog.open(TimesheetDialogComponent, {
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getAllTimesheet();
      }
    });
  }

  onEdit(data: any): void{

    console.log('print data', data);
    const dialogRef = this.dialog.open(TimesheetDialogComponent, {
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getAllTimesheet();
      }
    });
  }

  onDelete(id: string) {
    if (confirm("Confirm to delete") === false) {
      return; 
    }
    this.timesheetSvc.deleteTimesheet(id).subscribe({
      next: (resp:any) => {
        this.getAllTimesheet();
      },

      error: ()=>{}
    })
  }

  

}
