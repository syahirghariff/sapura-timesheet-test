import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class UserService {
    constructor(private http: HttpClient) {}

    private API_URL ='/api/v1/user';

    getAllUser() {
        return this.http.get(this.API_URL);
    }
}