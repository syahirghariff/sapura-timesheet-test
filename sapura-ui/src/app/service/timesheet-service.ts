import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class TimesheetService {
    constructor(private http: HttpClient) {}

    private API_URL ='/api/v1/timesheet';

    getAllTimesheet(task?: string) {
        
        let params: any = {};
        if (task) {
            params.task = task; 
        }
        return this.http.get(this.API_URL, {
            params: params
        });
    }

    saveTimesheet(request: any) {
        return this.http.post(this.API_URL, request);
    }

    deleteTimesheet(id: string) {
        return this.http.delete(this.API_URL+"/"+id);
    }
}