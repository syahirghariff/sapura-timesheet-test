import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class StatusService {
    constructor(private http: HttpClient) {}

    private API_URL ='/api/v1/status';

    getAllStatus() {
        return this.http.get(this.API_URL);
    }
}