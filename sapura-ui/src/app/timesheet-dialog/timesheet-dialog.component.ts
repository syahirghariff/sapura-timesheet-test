import { Component, Inject, OnInit } from '@angular/core';
import { UserService } from '../service/user-service';
import { StatusService } from '../service/status-service';
import { TimesheetService } from '../service/timesheet-service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-timesheet-dialog',
  templateUrl: './timesheet-dialog.component.html',
  styleUrl: './timesheet-dialog.component.css'
})
export class TimesheetDialogComponent implements OnInit {
  
  timesheet: any = {}; 
  users: any = null; 
  statuses: any = null; 

  constructor(private userService: UserService, 
    private statusService: StatusService,
    private timesheetService: TimesheetService,
    public dialogRef: MatDialogRef<TimesheetDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,) {
  }

  ngOnInit(): void {
      this.getUserList();
      this.getStatusList();
      this.checkData();
      console.log('syahir test data', this.data);

      
  }

  checkData(){

    if (!this.data) { return; }

    this.timesheet = {
      id: this.data.id,
      project: this.data.project,
      task: this.data.task,
      dateFrom : this.data.dateFrom, 
      dateTo: this.data.dateTo, 
      statusId: this.data.status.id,
      assignUserId: this.data.assignedTo.id
    }
  }

  getUserList(){
    this.userService.getAllUser().subscribe({
      next: (resp: any)=> {
          this.users = resp;
      },
      error: () => {}
    })
  }

  getStatusList(){
    this.statusService.getAllStatus().subscribe({
      next: (resp:any)=>{
        this.statuses = resp;
      }
    })
  }

  onSave(){
    if (confirm("Confirm to save") === false) {
      return; 
    }
    this.timesheetService.saveTimesheet(this.timesheet).subscribe({
      next: (resp:any) => {
        this.onClose(resp);
      }, 

      error: ()=>{}
    })
  }

  onClose(result?: any){

    let data:any = null;
    if (result) {
      data = {data : result}
    } 
    this.dialogRef.close(data);
  }



}
