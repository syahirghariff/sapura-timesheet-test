package com.sapura.timesheetservice.service;

import com.sapura.timesheetservice.model.Timesheet;
import com.sapura.timesheetservice.request.TimesheetReq;
import org.sapura.dto.TimesheetDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TimesheetService {

    List<TimesheetDto> getTimesheetByTask(String task);

    void saveTimesheet(TimesheetReq request);

    void deleteTimesheet(Integer id);

}
