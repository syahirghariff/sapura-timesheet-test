package com.sapura.timesheetservice.service;

import com.sapura.timesheetservice.converter.TimesheetConverter;
import com.sapura.timesheetservice.feignservice.StatusService;
import com.sapura.timesheetservice.feignservice.UserService;
import com.sapura.timesheetservice.model.Timesheet;
import com.sapura.timesheetservice.repository.TimesheetRepository;
import com.sapura.timesheetservice.request.TimesheetReq;
import jakarta.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.sapura.dto.StatusDto;
import org.sapura.dto.TimesheetDto;
import org.sapura.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
i
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class TimesheetServiceImpl implements TimesheetService {

    private final TimesheetRepository timesheetRepository;

    private final UserService userService;

    private final StatusService statusService;

    @Autowired
    public TimesheetServiceImpl(TimesheetRepository timesheetRepository,
                                UserService userService,
                                StatusService statusService) {
        this.timesheetRepository = timesheetRepository;
        this.userService = userService;
        this.statusService = statusService;
    }

    @Override
    public List<TimesheetDto> getTimesheetByTask(String task) {

        List<Timesheet> timesheetList = StringUtils.isEmpty(task)
                ? timesheetRepository.findAll()
                : timesheetRepository.findByTaskContaining(task);

        TimesheetConverter converter = new TimesheetConverter();
        return timesheetList.stream().map(converter::convert).collect(Collectors.toList());
    }

    @Override
    public void saveTimesheet(TimesheetReq request) {

        StatusDto statusDto = statusService.findById(request.getStatusId());

        UserDto userDto = userService.findById(request.getAssignUserId());

        Timesheet timesheet = Timesheet.builder()
                .project(request.getProject())
                .task(request.getTask())
                .userId(userDto.getId())
                .dateFrom(request.getDateFrom())
                .dateTo(request.getDateTo())
                .statusId(statusDto.getId())
                .build();

        Optional.ofNullable(request.getId())
                .map(NumberUtils::toInt)
                .ifPresent(timesheet::setId);

        timesheetRepository.save(timesheet);
    }

    @Override
    public void deleteTimesheet(Integer id) {
        timesheetRepository.deleteById(id);
    }
}
