package com.sapura.timesheetservice.model;


import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="timesheet")
@Builder
@AllArgsConstructor
public class Timesheet {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="project")
    private String project;

    @Column(name="task")
    private String task;

    @OneToOne
    @JoinColumn(name="assigned_user_id", insertable = false, updatable = false)
    private User assignedUser;

    @Column(name="assigned_user_id")
    private Integer userId;

    @Column(name="date_from")
    private LocalDate dateFrom;

    @Column(name="date_to")
    private LocalDate dateTo;

    @OneToOne
    @JoinColumn(name="status_id", insertable = false, updatable = false)
    private Status status;

    @Column(name="status_id")
    private Integer statusId;
}
