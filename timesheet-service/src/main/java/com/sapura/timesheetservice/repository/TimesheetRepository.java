package com.sapura.timesheetservice.repository;

import com.sapura.timesheetservice.model.Timesheet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimesheetRepository extends JpaRepository<Timesheet, Integer> {

    @Query("SELECT e FROM Timesheet e WHERE e.task LIKE %?1%")
    List<Timesheet> findByTaskContaining(String task);

}
