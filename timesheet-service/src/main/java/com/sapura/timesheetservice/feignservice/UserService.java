package com.sapura.timesheetservice.feignservice;

import org.sapura.dto.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("user-service")
public interface UserService {

    @GetMapping("/api/v1/user/{id}")
    UserDto findById(@PathVariable String id);
}

