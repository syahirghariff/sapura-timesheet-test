package com.sapura.timesheetservice.feignservice;

import org.sapura.dto.StatusDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("status-service")
public interface StatusService {

    @GetMapping("/api/v1/status/{id}")
    StatusDto findById(@PathVariable String id);
}
