package com.sapura.timesheetservice.controller;

import com.sapura.timesheetservice.model.Timesheet;
import com.sapura.timesheetservice.request.TimesheetReq;
import com.sapura.timesheetservice.response.RestResponse;
import com.sapura.timesheetservice.service.TimesheetService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.sapura.dto.TimesheetDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/timesheet")
public class TimesheetController {

    private final TimesheetService timesheetService;

    @Autowired
    public TimesheetController(TimesheetService timesheetService) {
        this.timesheetService = timesheetService;
    }

    @GetMapping
    public List<TimesheetDto> findAllTimesheet(@RequestParam(value = "task", required = false) String task) {
        return timesheetService.getTimesheetByTask(StringUtils.trim(task));
    }

    @PostMapping
    public ResponseEntity<RestResponse> save(@RequestBody TimesheetReq request ){
        timesheetService.saveTimesheet(request);
        return ResponseEntity.ok(new RestResponse("save success"));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<RestResponse> delete(@PathVariable Integer id){
        timesheetService.deleteTimesheet(id);
        return ResponseEntity.ok(new RestResponse("delete success"));
    }

}
