package com.sapura.timesheetservice.request;

import lombok.Data;

import java.time.LocalDate;

@Data
public class TimesheetReq {
    private String id;
    private String project;
    private String task;
    private String assignUserId;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private String statusId;
}
