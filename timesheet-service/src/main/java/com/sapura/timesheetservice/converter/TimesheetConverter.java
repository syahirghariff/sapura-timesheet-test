package com.sapura.timesheetservice.converter;

import com.sapura.timesheetservice.model.Status;
import com.sapura.timesheetservice.model.Timesheet;
import com.sapura.timesheetservice.model.User;
import org.sapura.dto.StatusDto;
import org.sapura.dto.TimesheetDto;
import org.sapura.dto.UserDto;
import org.springframework.core.convert.converter.Converter;

public class TimesheetConverter implements Converter<Timesheet, TimesheetDto> {

    @Override
    public TimesheetDto convert(Timesheet source) {
        return TimesheetDto.builder()
                .id(source.getId())
                .project(source.getProject())
                .task(source.getTask())
                .assignedTo(getUserDto(source.getAssignedUser()))
                .dateFrom(source.getDateFrom())
                .dateTo(source.getDateTo())
                .status(getStatusDto(source.getStatus()))
                .build();
    }

    private UserDto getUserDto (User user) {
        return UserDto.builder()
                .id(user.getId())
                .name(user.getName())
                .build();
    }

    private StatusDto getStatusDto (Status status){
        return StatusDto.builder()
                .id(status.getId())
                .name(status.getName())
                .description(status.getDescription())
                .build();
    }
}
