package com.sapura.statusservice.service;

import com.sapura.statusservice.model.Status;
import com.sapura.statusservice.repository.StatusRepository;
import com.sapura.statusservice.util.StatusDtoBuilder;
import org.sapura.dto.StatusDto;
import org.sapura.exception.RecordNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StatusServiceImpl implements StatusService{

    private final StatusRepository statusRepository;
    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository){
        this.statusRepository = statusRepository;
    }

    @Override
    public List<StatusDto> findAllStatus() {
        return statusRepository.findAll()
                .stream()
                .map(StatusDtoBuilder::buildStatusDto)
                .collect(Collectors.toList());
    }

    @Override
    public StatusDto findById(Integer id) {
        return statusRepository.findById(id)
                .map(StatusDtoBuilder::buildStatusDto)
                .orElseThrow(()-> new RecordNotFoundException("Status cannot be found"));
    }
}
