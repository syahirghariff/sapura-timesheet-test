package com.sapura.statusservice.service;

import org.sapura.dto.StatusDto;

import java.util.List;

public interface StatusService {
    List<StatusDto> findAllStatus();

    StatusDto findById(Integer id);
}
