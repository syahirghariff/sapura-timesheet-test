package com.sapura.statusservice.controller;

import com.sapura.statusservice.service.StatusService;
import org.apache.commons.lang3.math.NumberUtils;
import org.sapura.dto.StatusDto;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/status")
public class StatusController {

    private final StatusService statusService;

    public StatusController(StatusService statusService){
        this.statusService = statusService;
    }

    @GetMapping
    public List<StatusDto> findAllStatus() {
        return statusService.findAllStatus();
    }

    @GetMapping("/{id}")
    public StatusDto findStatusById(@PathVariable String id) {
        return statusService.findById(NumberUtils.toInt(id));
    }
}
