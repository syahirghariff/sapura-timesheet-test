package com.sapura.statusservice.util;

import com.sapura.statusservice.model.Status;
import org.sapura.dto.StatusDto;

public class StatusDtoBuilder {
    public static StatusDto buildStatusDto(Status s) {
        return StatusDto.builder()
                .id(s.getId())
                .name(s.getName())
                .description(s.getDescription())
                .build();
    }
}
